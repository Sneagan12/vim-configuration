#!/usr/bin/env bash

curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

brew install the_silver_searcher
brew install coreutils

nvimconfig=$(greadlink -f ~/.config/nvim)

if [ -d "$nvimconfig"  ]; then
  echo "Moving ~/.config/nvim to ~/.config/nvim.old"
  rm -rf ~/.config/nvim.old
  mv ~/.config/nvim ~/.config/nvim.old
fi

mkdir ~/.config/nvim
mkdir ~/.config/nvim/plugged
ln -s ~/.vim/nvimrc ~/.config/nvim/init.vim

