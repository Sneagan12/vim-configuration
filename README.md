# Jackson's Vim configuration

## Installation

    cd ~
    git clone git@bitbucket.org:Sneagan12/vim-configuration.git
    mv .vim .vim.old # only if you have a .vim file already
    mv vim-configuration .vim
    cd .vim
    git submodule init
    git submodule update
    ln -s vimrc ~/.vimrc

## Included

This installation sets up some basic conveniences like automatically creating matching braces and quotes in addition to some more advanced features. The more advanced stuff are things like JavaScript code highlighting, NerdTree (a directory explorer which I set to open on launch) and more

