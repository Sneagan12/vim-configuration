let g:polyglot_disabled = ['latex']
set rtp+=~/.fzf

call plug#begin('~/.config/nvim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle'  }
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-gitgutter'
Plug 'pangloss/vim-javascript'
Plug 'terryma/vim-multiple-cursors'
Plug 'jiangmiao/auto-pairs'
Plug 'itchyny/lightline.vim'
Plug 'w0rp/ale'
Plug 'editorconfig/editorconfig-vim'
Plug 'ternjs/tern_for_vim'
Plug 'tpope/vim-surround', {'autoload': {'filetypes': ['javascript']}}
Plug 'ggreer/the_silver_searcher'
Plug 'flowtype/vim-flow'
Plug 'mileszs/ack.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'rust-lang/rust.vim'
Plug 'sebastianmarkow/deoplete-rust'
Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'tomasr/molokai'
Plug 'danilo-augusto/vim-afterglow'
Plug 'junegunn/goyo.vim'
Plug 'python-mode/python-mode', {'branch': 'develop'}
Plug 'tpope/vim-abolish'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'sheerun/vim-polyglot'
Plug 'heavenshell/vim-pydocstring'
Plug 'godlygeek/tabular'
Plug 'jceb/vim-orgmode'
Plug 'lervag/vimtex'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
call plug#end()

let mapleader=" "

" Set F9 to toggle taglist
nnoremap <silent> <F9> :TlistToggle<CR>
let g:Tlist_Ctags_Cmd='/usr/local/bin/ctags'  " Proper ctags location

" Fix Arch Flickering
set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50

" Find color themes in the .vim directory
set rtp+=~/.vim

" fzf bindings
nmap ; :Buffers<CR>
nmap <Leader>t :Files<CR>
nmap <Leader>r :Tags<CR>

" One theme configuration (background has to come after colorscheme)
set background=dark
set termguicolors
"colorscheme molokai
colorscheme afterglow

" Share system clipboard
set clipboard+=unnamedplus

" Set encoding to UTF-* to show glyphs
set encoding=utf8

" Automatically reload buffers when file changes on disk
set autoread

" Set current column indicator
set cursorcolumn
set cursorline
"autocmd InsertEnter * highlight CursorColumn ctermfg=Black ctermbg=DarkGray cterm=bold guifg=Black guibg=darkgrey gui=NONE
"autocmd InsertLeave * highlight CursorColumn ctermfg=Black ctermbg=DarkGray cterm=bold guifg=Black guibg=darkgrey gui=NONE
"autocmd InsertEnter * highlight CursorLine ctermfg=Black ctermbg=DarkGray cterm=bold guifg=Black guibg=darkgrey gui=NONE
"autocmd InsertLeave * highlight CursorLine ctermfg=Black ctermbg=DarkGray cterm=bold guifg=Black guibg=darkgrey gui=NONE
if has("autocmd")
  au VimEnter,InsertLeave * silent execute '!echo -ne "\e[2 q"' | redraw!
  au InsertEnter,InsertChange *
    \ if v:insertmode == 'i' | 
    \   silent execute '!echo -ne "\e[6 q"' | redraw! |
    \ elseif v:insertmode == 'r' |
    \   silent execute '!echo -ne "\e[4 q"' | redraw! |
    \ endif
  au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
endif

" Show white space
set list
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣

" neovim-remote support for vimtex
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
let g:vimtex_view_general_options_latexmk = '--unique'

" Python Mode
let g:pymode_python = 'python3'
let g:pymode_rope_complete_on_dot = 0
"let g:python3_host_prog  = '/usr/local/bin/python3'

" Markdown Config
let g:vim_markdown_folding_disabled = 1

" Set hybrid line numbers
set number relativenumber

" enable ag
let g:ackprg = 'ag --nogroup --nocolor --column'

" Use ag and fzf to ignore searching gitignored files
let $FZF_DEFAULT_COMMAND = 'ag -g ""'

" Set folding
set foldmethod=indent
set foldlevelstart=10

let javaScript_fold=1         " JavaScript
let perl_fold=1               " Perl
let php_folding=1             " PHP
let r_syntax_folding=1        " R
let ruby_fold=1               " Ruby
let sh_fold_enabled=1         " sh
let vimsyn_folding='af'       " Vim script
let xml_syntax_folding=1      " XML

filetype on
syntax on
" set guifont=JetBrains Mono:h18
set colorcolumn=90
set rnu " Relative line numbers
set hidden
set history=100
filetype indent on
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set shiftwidth=4
set smartindent
set autoindent
set hlsearch
set showmatch
set wildignore+=*.log,*.sql,*.cache
set t_Co=256

nnoremap <Leader><Leader> :e#<CR>
nnoremap <Leader>r :CommandTFlush<CR>
nnoremap <C-p> :FZF<CR>
xnoremap p pgvy

let signcolumn=1
let g:javascript_plugin_flow = 1

" Deoplete settings
let g:deoplete#enable_at_startup = 1

" deoplete tab-complete
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
" tern
autocmd FileType javascript nnoremap <silent> <buffer> gb :TernDef<CR>

" let g:deoplete#disable_auto_complete = 1
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" omnifuncs
augroup omnifuncs
  autocmd!
  autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
  autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
  autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
  autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
  autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
augroup end
" tern
if exists('g:plugs["tern_for_vim"]')
  let g:tern_show_argument_hints = 'on_hold'
  let g:tern_show_signature_in_pum = 1
  autocmd FileType javascript setlocal omnifunc=tern#Complete
endif


let NERDTreeMapActivateNode='<right>'
let NERDTreeShowHidden=1
nmap <leader>n :NERDTreeToggle<CR>
nmap <leader>j :NERDTreeFind<CR>
" autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

let NERDTreeIgnore=['\.DS_Store', '\~$', '\.swp']

map <leader>s :source ~/.vimrc<CR>
map <D-A-RIGHT> <C-w>l
map <D-A-LEFT> <C-w>h
map <D-A-DOWN> <C-w><C-w>
map <D-A-UP> <C-w>W

" Transparent Background
hi Normal guibg=NONE ctermbg=NONE


"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

imap jj <Esc>
